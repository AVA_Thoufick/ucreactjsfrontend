import { Dispatcher } from 'simplr-flux';
import Constants from '../Constants';
import * as APIService from "../Service";

const PageLoad=()=>{
    try{
        APIService.getCallGateway(
            Constants.URL.formLoadURL,
            (data)=>
            {
               
                if(data.success)
                {
                    
                    if(data.response!="" && data.response)
                    {
                        let contentdata={ type:Constants.URL.formLoadURL,
                                          response:data.response
                                        }
                                       
                                        
                                        Dispatcher.dispatch(contentdata);
                    }
                }
            }
        );
    }
    catch(ex)
    {

    }
}

const PostData=(postdata)=>
{
    console.log(postdata);
    try{
      
        APIService.postCallGateway(
            Constants.URL.formPostDataURL,
            postdata,
            (data)=>
            {
             
                if(data.success)
                {
                    
                    if(data.response!="" && data.response)
                    {
                        
                        let contentdata={ type:Constants.URL.formPostDataURL,
                                          response:data.response
                                        }
                                    
                                        Dispatcher.dispatch(contentdata);
                    }
                }
            }
        );
    }
    catch(ex)
    {

    }
}


const updateFileName=(postdata)=>
{
    console.log(postdata);
    try{
      
        APIService.postCallGateway(
            Constants.URL.outputFileNameUpdate,
            postdata,
            (data)=>
            {
             
                if(data.success)
                {
                    
                    if(data.response!="" && data.response)
                    {
                        
                        let contentdata={ type:Constants.URL.outputFileNameUpdate,
                                          response:data.response
                                        }
                                    
                                        Dispatcher.dispatch(contentdata);
                    }
                }
            }
        );
    }
    catch(ex)
    {

    }
}

const callGoAPI=(GoPostData)=>
{
    console.log(GoPostData);
    try{
      
        APIService.postCallGateway(
            Constants.GO.CodeGenURL,
            GoPostData,
            (data)=>
            {
             
                if(data.success)
                {
                    
                    if(data.response!="" && data.response)
                    {
                        
                        let contentdata={ type:Constants.GO.CodeGenURL,
                                          response:data.response
                                        }
                                    
                                        Dispatcher.dispatch(contentdata);
                    }
                }
            }
        );
    }
    catch(ex)
    {

    }
}

const getData=(postdata)=>
{
    try{
    //    console.log(formGetDataURL)
        APIService.postCallGateway(
            Constants.URL.formGetDataURL,
            postdata,
            (data)=>
            {
                
                if(data.success)
                {
                    
                    if(data.response!="" && data.response)
                    {
                        
                        let contentdata={ type:Constants.URL.formGetDataURL,
                                          response:data.response
                                        }
                                        
                                        Dispatcher.dispatch(contentdata);
                    }
                }
            }
        );
    }
    catch(ex)
    {

    }
}

export {PostData, PageLoad, getData,callGoAPI , updateFileName}

